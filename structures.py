import warnings
from typing import Any, List, NamedTuple, Union

from PyQt5 import QtWidgets
from PyQt5.QtCore import QCoreApplication
from attrdict import AttrDict

_translate = QCoreApplication.translate


class Option(NamedTuple):
    label: str
    value: Any


class OptZoom:
    ADAPTIVE = Option(
        _translate('MainWindow', 'Adaptive Zoom (No borders)'), 2)
    NONE = Option(_translate('MainWindow', 'No Zooming'), 0)
    STATIC = Option(
        _translate('MainWindow', 'Static Zoom (Possible borders)'), 1)


class Settings:
    BIN_FFMPEG = 'bin/ffmpeg'
    STAB_ACCURACY = 'stabilize/accuracy'
    STAB_OPTZOOM = 'stabilize/optzoom'
    STAB_REUSE_TRANSFORMS = 'stabilize/reuse_transforms'
    STAB_ROTATION = 'stabilize/rotation'
    STAB_SHAKINESS = 'stabilize/shakiness'
    STAB_SMOOTHING = 'stabilize/smoothing'
    STAB_STEP_SIZE = 'stabilize/step_size'
    STAB_TRANSLATION = 'stabilize/translation'


class Defaults:
    STAB_ACCURACY = 15
    STAB_OPTZOOM = OptZoom.STATIC.value
    STAB_REUSE_TRANSFORMS = False
    STAB_ROTATION = -1
    STAB_SHAKINESS = 5
    STAB_SMOOTHING = 10
    STAB_STEP_SIZE = 6
    STAB_TRANSLATION = -1


class BinState:
    ffmpeg: str


class StabState(AttrDict):
    accuracy: int
    optzoom: int
    reuse_transforms: bool
    rotation: float
    shakiness: int
    smoothing: int
    step_size: int
    translation: int


class AppState(AttrDict):
    bin: BinState
    files: List[str]
    stabilize: StabState


class JobMeta(NamedTuple):
    cmd: List[str]
    file: str
    name: str
    step: int
    total: int


class _Filter(NamedTuple):
    default: float
    max: float
    min: float
    precision: int
    type: str

    @classmethod
    def factory(cls, config: dict):
        defaults = dict(
            default=0,
            max=None,
            min=None,
            precision=None,
            type='float',
        )
        defaults.update(config)

        return cls(**defaults)


class Filter:
    config: _Filter
    control: Any
    name: str

    def __init__(
            self,
            name: str,
            config: dict,
            control: Any = None,
            widget: QtWidgets.QWidget = None):
        self.name = name

        config_ = _Filter.factory(config)
        self.config = config_

        if widget:
            self.control = self.control_factory(config_, widget)
        else:
            self.control = control

    @classmethod
    def control_factory(
            cls,
            config: _Filter,
            widget: QtWidgets.QWidget,
    ) -> Union[QtWidgets.QSpinBox, QtWidgets.QDoubleSpinBox]:
        control = cls.control_picker(config.type)(widget)
        if not control:
            return control

        if config.max is not None and config.min is not None:
            if config.min >= config.max:
                warnings.warn(
                    'Filter minimum is greater than or equal to maximum. '
                    'This is invalid.')

                return control

        if config.min is not None:
            control.setMinimum(config.min)
        else:
            control.setMinimum(-2147483647)

        if config.max is not None:
            control.setMaximum(config.max)
        else:
            control.setMaximum(2147483647)

        if config.precision is not None:
            if hasattr(control, 'setDecimals'):
                control.setDecimals(config.precision)
            else:
                warnings.warn(
                    'Control does not support setDecimals, '
                    'ignoring precision.'
                )

        control.setValue(config.default)

        return control

    @classmethod
    def control_picker(cls, type_: str):
        if type_ == 'int':
            return QtWidgets.QSpinBox

        if type_ == 'flag':
            return lambda c: None

        return QtWidgets.QDoubleSpinBox

    @property
    def tooltip(self):
        conf = self.config

        if conf.min is not None and conf.max is not None:
            return (
                '{} to {}. Default {}.'
                .format(str(conf.min), str(conf.max), str(conf.default))
            )

        if conf.min:
            return (
                'Minimum value {}. Default {}.'
                .format(str(conf.min), str(conf.default))
            )

        if conf.max:
            return (
                'Maximum value {}. Default {}.'
                .format(str(conf.max), str(conf.default))
            )

        return 'Default {}.'.format(str(conf.default))

    def cast_value(self, value):
        if self.config.type == 'int':
            return int(value)

        return float(value or 0)
