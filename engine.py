from concurrent.futures import ThreadPoolExecutor
from queue import Queue, Empty
from subprocess import Popen, PIPE
from typing import BinaryIO, Callable, List, Generator, Tuple, Union

from structures import AppState, Filter

ENCODING = 'utf-8'


def _get_clean_value(value):
    return value.decode(ENCODING).strip()


def _enqueue_output(file: BinaryIO, queue: Queue):
    for line in iter(file.readline, b''):
        queue.put(_get_clean_value(line))

    file.close()


def _read_popen_pipes(proc: Popen):
    with ThreadPoolExecutor(2) as pool:
        q_stdout, q_stderr = Queue(), Queue()

        pool.submit(_enqueue_output, proc.stdout, q_stdout)
        pool.submit(_enqueue_output, proc.stderr, q_stderr)

        while True:
            if all([
                proc.poll() is not None,
                q_stdout.empty(),
                q_stderr.empty()
            ]):
                break

            err_line = ''
            out_line = ''

            try:
                out_line = q_stdout.get_nowait()
            except Empty:
                pass

            try:
                err_line = q_stderr.get_nowait()
            except Empty:
                pass

            if not any([out_line, err_line]):
                continue

            yield out_line, err_line


def _build_filter(name: str, configs: List[Filter]) -> str:
    configurations = map(
        lambda conf: '{}={}'.format(conf.name, conf.control.value()),
        filter(lambda conf: conf.config.type != 'flag', configs)
    )
    config = ':'.join(configurations)

    if config:
        return '{}={}'.format(name, config)

    return name


def _build_stab_detect_filter(state: AppState, file: str) -> str:
    return (
        '[0:v]yadif,'
        'vidstabdetect=stepsize={state.stabilize.step_size}'
        ':shakiness={state.stabilize.shakiness}'
        ':accuracy={state.stabilize.accuracy}'
        ':result={file}.trf'
        .format(file=file, state=state)
    )


def _build_stab_filter(state: AppState, file: str) -> str:
    return (
        '[0:v]yadif,'
        'vidstabtransform=optzoom={state.stabilize.optzoom}'
        ':smoothing={state.stabilize.smoothing}'
        ':input={file}.trf'
        .format(file=file, state=state)
    )


def execute(
        command: List,
        *,
        stderr_emitter: Callable,
        stdout_emitter: Callable,
        context_switcher: Callable,
) -> Generator[Union[Popen, int], None, None]:
    with Popen(command, stdout=PIPE, stderr=PIPE) as proc:
        yield proc

        for out, err in _read_popen_pipes(proc):
            if context_switcher:
                context_switcher()

            if out and stdout_emitter:
                stdout_emitter(out)

            if err and stderr_emitter:
                stderr_emitter(err)

        code = proc.poll()

    yield code


def stab_detect_cmd(state: AppState, file: str) -> List[str]:
    return [
        state.bin.ffmpeg,
        '-nostdin',
        # Input file.
        '-i', file,
        # Video filter.
        '-vf', _build_stab_detect_filter(state, file),
        # No audio.
        '-an',
        # No output file.
        '-f', 'null',
        '-',
    ]


def stabilize_cmd(
        state: AppState,
        file: str,
        filter_pipeline: List[Tuple[str, Filter]]) -> List[str]:
    stab_filter = _build_stab_filter(state, file)
    filters = map(
        lambda fp: _build_filter(fp[0], fp[1]),
        filter_pipeline)
    all_filters = [stab_filter] + list(filters)
    filter_str = ','.join(all_filters)

    return [
        state.bin.ffmpeg,
        '-nostdin',
        '-i', file,
        '-y',
        '-vcodec', 'mpeg4',
        '-b:v', '15M',
        '-acodec', 'copy',
        '-filter_complex',
        filter_str,
        file + '.stable.mp4'
    ]
