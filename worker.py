import sys
import traceback
from typing import Callable

from PyQt5.QtCore import (
    pyqtSignal,
    pyqtSlot,
    QObject,
    QRunnable,
)
from PyQt5.QtWidgets import QApplication


class LogChannel(QObject):
    stdout = pyqtSignal(str)
    stderr = pyqtSignal(str)


class WorkerSignal(QObject):
    finished = pyqtSignal()
    exit_code = pyqtSignal(int)


class Worker(QRunnable):
    """
    https://www.learnpyqt.com/courses/concurrent-execution/multithreading-pyqt-applications-qthreadpool/
    """
    def __init__(
            self,
            fn: Callable,
            *args,
            stderr_emitter: Callable,
            stdout_emitter: Callable,
            **kwargs):
        super().__init__()

        self.fn = fn
        self.args = args
        self.kwargs = kwargs

        self.stderr = stderr_emitter
        self.stdout = stdout_emitter

        self.proc = None
        self.signals = WorkerSignal()

    @pyqtSlot()
    def run(self):
        """
        Initialise the runner function with passed args, kwargs.
        """

        exit_code = -99
        try:
            process = self.fn(
                *self.args,
                context_switcher=QApplication.processEvents,
                stderr_emitter=self.stderr,
                stdout_emitter=self.stdout,
                **self.kwargs)
            self.proc = next(process)
            QApplication.processEvents()
            exit_code = next(process)
            QApplication.processEvents()

        except:
            traceback.print_exc()
            self.stderr(str(sys.exc_info()))
            exit_code = 99

        finally:
            self.signals.exit_code.emit(exit_code)

        self.signals.finished.emit()

    def terminate(self):
        if self.proc and hasattr(self.proc, 'kill'):
            self.stdout('Terminating')
            self.proc.kill()
