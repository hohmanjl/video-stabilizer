import os
import re
import warnings
from functools import partial
from time import perf_counter
from typing import Iterator, List

import pydash
import yaml
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import (
    QEvent,
    QEventLoop,
    QObject,
    QSettings,
    QThreadPool,
    QTimer,
    QUrl,
)
from PyQt5.QtGui import QColorConstants
from PyQt5.QtWidgets import QApplication, QFileDialog, QStyle, QMessageBox
from attrdict import AttrDict

import auto_ui
from engine import execute, stabilize_cmd, stab_detect_cmd
from structures import AppState, Defaults, Filter, JobMeta, OptZoom, Settings
from worker import LogChannel, Worker

APP_NAME = 'video_stabilizer'


class Window(QtWidgets.QMainWindow, auto_ui.Ui_MainWindow):
    FILES_LIST_DROP_EVENTS = {QEvent.DragEnter, QEvent.Drop}
    FILES_LIST_KEY_EVENTS = {QEvent.KeyPress}

    RE_MISSING_VIDSTAB = ".*No such filter: 'vidstabdetect'.*"
    RE_PERMISSION_DENIED = '.*Permission denied.*'

    UPDATE_MS = 250

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.path_ffmpeg = ''
        self.settings = QSettings(
            '{}.ini'.format(APP_NAME),
            QSettings.IniFormat)

        self.files_model = QtGui.QStandardItemModel()

        # Must load filters first.
        self.filters = AttrDict()
        self.filter_pipeline = []
        self.load_filter_configs()

        self.init_models()
        self.init_handlers()
        self.init_style()

        self.load_settings()

        self.files_list.installEventFilter(self)

        self.thread_pool = QThreadPool()

        self.channel = LogChannel()
        self.channel.stdout.connect(self.log_output)
        self.channel.stderr.connect(self.log_error)

        self.default_log_color = self.log_view.textColor()

        self.worker = None
        self.worker_exit_code = None
        self.exiting = False

    def _is_permission_error(self, output):
        return bool(re.search(self.RE_PERMISSION_DENIED, output))

    def _is_missing_vidstab_error(self, output):
        return bool(re.search(self.RE_MISSING_VIDSTAB, output))

    def closeEvent(self, event):
        self.exiting = True
        if self.worker:
            self.worker.terminate()
            self.worker = None

        last_state = self.collect_state()
        self.save_settings(last_state)

        QApplication.processEvents()

        event.accept()

    def log_output(self, message: str) -> None:
        self.log_view.append(message)

    def log_error(self, message: str) -> None:
        self.log_view.setTextColor(QColorConstants.Gray)
        self.log_view.append(message)
        self.log_view.setTextColor(self.default_log_color)

    def init_handlers(self) -> None:
        self.add_files_button.clicked.connect(self.handle_add_files_button)
        self.add_filter_control.activated.connect(self.handle_filter_add)
        self.clear_files_button.clicked.connect(self.handle_clear_files_button)
        self.ffmpeg_button.clicked.connect(self.handle_ffmpeg_bin)
        self.filters_widget.tabBar().tabMoved.connect(
            self.handle_filter_move)
        self.filters_widget.tabCloseRequested.connect(
            self.handle_filter_remove)
        self.process_button.clicked.connect(self.handle_process)

    def build_form(self, tab: QtWidgets.QWidget) -> List[Filter]:
        config = self.filters[tab.objectName()]
        horizontal = QtWidgets.QHBoxLayout(tab)
        layout = QtWidgets.QFormLayout()

        controls = []
        for i, (widget, conf_) in enumerate(config.conf.items()):
            filter_ = Filter(widget, conf_, widget=tab)

            label = QtWidgets.QLabel(tab)
            layout.setWidget(i, QtWidgets.QFormLayout.LabelRole, label)
            label.setText(widget)
            label.setToolTip(filter_.tooltip)

            control = filter_.control
            if control:
                control.setToolTip(filter_.tooltip)
                layout.setWidget(i, QtWidgets.QFormLayout.FieldRole, control)

            controls.append(filter_)

        horizontal.addLayout(layout)

        return controls

    def filter_add(self, filter_name: str) -> List[Filter]:
        filter_tab = QtWidgets.QWidget()
        filter_tab.setObjectName(filter_name)
        filters = self.build_form(filter_tab)
        self.filters_widget.addTab(filter_tab, filter_name)

        self.filter_pipeline.append((filter_name, filters))

        return filters

    def handle_filter_add(self, _i: int) -> None:
        filter_name = self.add_filter_control.currentText()
        if not filter_name:
            return

        self.load_filter(filter_name)

    def handle_filter_remove(self, i: int) -> None:
        self.filters_widget.removeTab(i)

        # The first tab cannot be removed, thus indices are always i + 1
        # with regard to the filter pipeline.
        idx = i - 1
        self.save_filter(*self.filter_pipeline[idx])
        del self.filter_pipeline[idx]

    def handle_filter_move(self, fr: int, to: int) -> None:
        old = fr - 1
        new = to - 1
        filter_ = self.filter_pipeline.pop(old)
        self.filter_pipeline.insert(new, filter_)

    def init_models(self) -> None:
        self.files_list.setModel(self.files_model)

        self.optzoom_control.addItem(
            OptZoom.NONE.label, OptZoom.NONE.value)
        self.optzoom_control.addItem(
            OptZoom.STATIC.label, OptZoom.STATIC.value)
        self.optzoom_control.addItem(
            OptZoom.ADAPTIVE.label, OptZoom.ADAPTIVE.value)

        self.add_filter_control.addItem(None)
        for filter_ in self.filters.values():
            self.add_filter_control.addItem(filter_.bin)

    def init_style(self) -> None:
        icon_open_file = self.style().standardIcon(QStyle.SP_FileDialogStart)
        self.ffmpeg_button.setIcon(icon_open_file)
        self.add_files_button.setIcon(icon_open_file)

        icon_trash = self.style().standardIcon(QStyle.SP_TrashIcon)
        self.clear_files_button.setIcon(icon_trash)

        # icon_title = self.style().standardIcon(QStyle.SP_TitleBarMenuButton)
        # self.setWindowIcon(icon_title)

        # Disable closing the "add filter" tab.
        self.filters_widget.tabBar().setTabButton(
            0, QtWidgets.QTabBar.RightSide, None)

    def load_filter(self, filter_name: str):
        filter_conf = self.filters.get(filter_name, None)
        if not filter_conf:
            warnings.warn('Filter does not exist! {}'.format(filter_name))
            return

        filters = self.filter_add(filter_name)

        self.settings.beginGroup('filters')
        self.settings.beginGroup(filter_name)
        child_keys = self.settings.childKeys()

        for key in child_keys:
            filter_ = pydash.find(filters, lambda f: f.name == key)
            if not filter_:
                warnings.warn('Filter config does not exist! {}'.format(key))
                continue

            if filter_.control:
                value = self.settings.value(key, filter_.config.default)
                filter_.control.setValue(filter_.cast_value(value))

        self.settings.endGroup()
        self.settings.endGroup()

    def save_filter(self, filter_name, filters):
        self.settings.beginGroup('filters')
        self.settings.beginGroup(filter_name)
        self.settings.remove('')

        for filter_ in filters:
            if filter_.control:
                self.settings.setValue(
                    filter_.name,
                    filter_.control.value())
            else:
                self.settings.setValue(filter_.name, '')

        self.settings.endGroup()
        self.settings.endGroup()
        self.settings.sync()

    def load_filter_configs(self) -> None:
        filters_dir = os.path.join(os.curdir, 'filters')

        filters = []
        for file_obj in os.listdir(filters_dir):
            file = os.path.join(filters_dir, file_obj)
            if os.path.isfile(file):
                filters.append(file)

        filters.sort()
        for f in filters:
            with open(f, 'r') as handle:
                try:
                    filter_ = yaml.safe_load(handle)
                except yaml.YAMLError as exc:
                    warnings.warn(
                        'Warning could not load {}. Reason: {}.'
                        .format(f, str(exc))
                    )
                    continue

                filter_ = AttrDict(filter_)
                self.filters[filter_.bin] = filter_

    def load_settings(self) -> None:
        get_value = self.settings.value

        path_ffmpeg = get_value(Settings.BIN_FFMPEG, '')
        accuracy = get_value(Settings.STAB_ACCURACY, Defaults.STAB_ACCURACY)
        optzoom = get_value(Settings.STAB_OPTZOOM, Defaults.STAB_OPTZOOM)
        reuse_transforms = get_value(
            Settings.STAB_REUSE_TRANSFORMS, Defaults.STAB_REUSE_TRANSFORMS)
        rotation = get_value(Settings.STAB_ROTATION, Defaults.STAB_ROTATION)
        shakiness = get_value(Settings.STAB_SHAKINESS, Defaults.STAB_SHAKINESS)
        smoothing = get_value(Settings.STAB_SMOOTHING, Defaults.STAB_SMOOTHING)
        step_size = get_value(Settings.STAB_STEP_SIZE, Defaults.STAB_STEP_SIZE)
        translation = get_value(
            Settings.STAB_TRANSLATION, Defaults.STAB_TRANSLATION)

        self.set_ffmpeg_bin(path_ffmpeg)
        self.accuracy_control.setValue(float(accuracy))
        self.set_optzoom(optzoom)
        self.reuse_transforms_control.setChecked(int(reuse_transforms))
        self.rotation_control.setValue(float(rotation))
        self.shakiness_control.setValue(int(shakiness))
        self.smoothing_control.setValue(int(smoothing))
        self.step_size_control.setValue(int(step_size))
        self.translation_control.setValue(int(translation))

        for filter_ in get_value('active/filters', []):
            self.load_filter(filter_)

    def save_settings(self, state: AttrDict) -> None:
        set_value = self.settings.setValue

        set_value(Settings.BIN_FFMPEG, state.bin.ffmpeg)
        set_value(Settings.STAB_ACCURACY, state.stabilize.accuracy)
        set_value(Settings.STAB_OPTZOOM, state.stabilize.optzoom)
        set_value(
            Settings.STAB_REUSE_TRANSFORMS, state.stabilize.reuse_transforms)
        set_value(Settings.STAB_ROTATION, state.stabilize.rotation)
        set_value(Settings.STAB_SHAKINESS, state.stabilize.shakiness)
        set_value(Settings.STAB_SMOOTHING, state.stabilize.smoothing)
        set_value(Settings.STAB_STEP_SIZE, state.stabilize.step_size)
        set_value(Settings.STAB_TRANSLATION, state.stabilize.translation)

        active_filters = list(map(lambda fp: fp[0], self.filter_pipeline))
        if active_filters:
            set_value('active/filters', active_filters)
        else:
            set_value('active/filters', '')

        self.settings.beginGroup('filters')
        for filter_name, filters in self.filter_pipeline:
            self.settings.beginGroup(filter_name)
            for filter_ in filters:
                if filter_.control:
                    set_value(filter_.name, filter_.control.value())
                else:
                    set_value(filter_.name, '')
            self.settings.endGroup()
        self.settings.endGroup()

        self.settings.sync()

    def eventFilter(self, source: QObject, event: QEvent) -> bool:
        return any([
            self.handle_file_drop(source, event),
            self.handle_file_keypress(source, event),
        ])

    def collect_files(self) -> List[str]:
        model = self.files_list.model()
        files = []
        for idx in range(model.rowCount()):
            item = model.item(idx)
            files.append(item.text())

        return files

    def collect_state(self) -> AppState:
        return AttrDict(
            bin=dict(
                ffmpeg=self.ffmpeg_control.text(),
            ),
            stabilize=dict(
                accuracy=self.accuracy_control.value(),
                optzoom=self.optzoom_control.currentData(),
                reuse_transforms=self.reuse_transforms_control.checkState(),
                rotation=self.rotation_control.value(),
                shakiness=self.shakiness_control.value(),
                smoothing=self.smoothing_control.value(),
                step_size=self.step_size_control.value(),
                translation=self.translation_control.value(),
            ),
            files=self.collect_files(),
        )

    def handle_add_files_button(self) -> None:
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.ExistingFiles)

        files, filter_ = dialog.getOpenFileNames(
            self,
            'Select Media')

        QApplication.processEvents()

        self.set_files(files)

    def handle_clear_files_button(self) -> None:
        self.files_model.clear()

    def handle_ffmpeg_bin(self) -> None:
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.ExistingFile)

        file_path, filter_ = dialog.getOpenFileName(
            self,
            'Select ffmpeg Binary',
            QtCore.QDir.rootPath())

        self.set_ffmpeg_bin(file_path)

    def handle_file_drop(self, source: QObject, event: QEvent) -> bool:
        if source is not self.files_list:
            return False

        event_type = event.type()
        if event_type not in self.FILES_LIST_DROP_EVENTS:
            return False

        has_urls = event.mimeData().hasUrls()
        if not has_urls:
            return False

        if event_type == QEvent.DragEnter:
            event.accept()
            return True

        if event_type == QEvent.Drop:
            urls = event.mimeData().urls()
            files = self.map_urls_to_files(urls)
            self.set_files(files)

        return False

    def handle_file_keypress(self, source: QObject, event: QEvent) -> bool:
        if source is not self.files_list:
            return False

        event_type = event.type()
        if event_type not in self.FILES_LIST_KEY_EVENTS:
            return False

        key = event.key()
        if not key:
            return False

        if key == QtCore.Qt.Key_Delete:
            self.remove_files()
            return True

        return False

    def _handle_perm_error(self) -> None:
        self.log_view.append('')
        self.log_view.append(
            '<div style="color: red;">'
            '<hr/>'
            '<p>Error!</p>'
            '<p>'
            'This appears to be a file permission error.'
            '</p>'
            '<p>'
            'If you are using Snap Package Manager, it does not '
            'give ffmpeg access to removable media. '
            "You'll need to give it access if you want to do "
            'operations on removable media. '
            'You can try: '
            '</p>'
            '<p>'
            '<code>'
            'sudo snap connect ffmpeg:removable-media'
            '</code>'
            '</p>'
            '</div>'
        )

    def _handle_vidstab_error(self) -> None:
        self.log_view.append('')
        self.log_view.append(
            '<div style="color: red;">'
            '<hr/>'
            '<p>Error!</p>'
            '<p>'
            'It appears the your version of ffmpeg does not have '
            'vidstab compiled. '
            ''
            'See: https://ffmpeg.org/ffmpeg-filters.html#vidstabdetect-1 '
            '</p>'
            '<p>'
            'Alternatively, install a compiled binary. '
            ''
            'https://www.johnvansickle.com/ffmpeg/'
            '</p>'
            '</div>'
        )

    def set_worker_code(self, code: int) -> None:
        self.worker_exit_code = code

    def handle_result(
            self,
            job: JobMeta,
            exit_code: int,
            stderr: str) -> bool:
        if exit_code == 0:
            self.log_view.append('Step {}. Complete!'.format(job.step))

            # No errors.
            return False

        self.log_view.setTextColor(QColorConstants.Red)
        self.log_view.append('Step {}. Error!'.format(job.step))
        self.log_view.setTextColor(self.default_log_color)

        if self._is_permission_error(stderr):
            self._handle_perm_error()

        elif self._is_missing_vidstab_error(stderr):
            self._handle_vidstab_error()

        self.log_view.append('')

        # Has errors.
        return True

    def handle_finished(self, msg):
        self.log_view.append('')
        self.log_view.append(msg)

        self.worker = None

    def handle_stderr(self, history: list, msg: str) -> None:
        self.channel.stderr.emit(msg)
        history.append(msg)

    def handle_stdout(self, history: list, msg: str) -> None:
        self.channel.stdout.emit(msg)
        history.append(msg)

    def run_job(self, job: JobMeta, start: float = None) -> bool:
        if self.exiting:
            return False

        if not start:
            start = perf_counter()

        stderr_history = []
        stdout_history = []
        stderr = partial(self.handle_stderr, stderr_history)
        stdout = partial(self.handle_stdout, stdout_history)

        self.worker_exit_code = None
        self.worker = Worker(
            execute,
            job.cmd,
            stderr_emitter=stderr,
            stdout_emitter=stdout,
        )
        on_finish = partial(
            self.handle_finished,
            '{job.name} finished.'.format(job=job))
        self.worker.signals.finished.connect(on_finish)
        self.worker.signals.exit_code.connect(self.set_worker_code)

        # Execute
        self.thread_pool.start(self.worker)

        while self.worker:
            loop = QEventLoop()
            QTimer.singleShot(self.UPDATE_MS, loop.quit)
            loop.exec()
            elapsed = perf_counter() - start
            self.statusbar.showMessage(
                '{job_name} {job.file}, '
                'Step {job.step:d}/{job.total:d}... '
                '{elapsed:d}s'
                .format(
                    job_name=job.name.title(),
                    job=job,
                    elapsed=round(elapsed)))

        if self.exiting:
            return False

        if self.worker_exit_code is None:
            raise RuntimeError('Worker failed to set exit code!')

        std_errors = '\n'.join(stderr_history)
        return self.handle_result(job, self.worker_exit_code, std_errors)

    def should_skip_transform(self, file: str) -> bool:
        if not self.reuse_transforms_control.isChecked():
            return False

        if not os.path.exists(file):
            return False

        if not os.path.isfile(file):
            return False

        self.log_view.append('Transform exists, skipping...')

        return True

    def log_job_metadata(self, job: JobMeta, cmd: List[str]) -> None:
        self.log_view.append(
            'Step {step}. {name}'
            .format(step=job.step, name=job.name.title())
        )
        self.log_view.append(
            '<p>'
            'Command'
            '<br/>'
            '<code>{}</code>'
            '</p>'
            .format(' '.join(cmd))
        )

    def handle_process(self) -> None:
        self.log_view.clear()
        state = self.collect_state()
        try:
            self.validate_params(state)
        except Exception as e:
            message = e.args[0]
            self.show_error_dialog('Error!', message.type, message.detail)
            return

        status_message = self.statusbar.showMessage
        status_message('')

        has_errors = False
        start = perf_counter()
        for file in state.files:
            if self.exiting:
                return

            self.log_view.append(
                '<hr/>'
                '<p>'
                '<h1>Process file {}</h1>'
                '</p>'
                .format(file)
            )
            cmd = stab_detect_cmd(state, file)
            job = JobMeta(
                cmd=cmd,
                file=file,
                name='generate transform',
                step=1,
                total=2,
            )
            self.log_job_metadata(job, cmd)
            if not self.should_skip_transform(file):
                if self.run_job(job, start=start):
                    has_errors = True
                    continue

            cmd = stabilize_cmd(state, file, self.filter_pipeline)
            job = JobMeta(
                cmd=cmd,
                file=file,
                name='stabilize video',
                step=2,
                total=2,
            )
            self.log_job_metadata(job, cmd)
            if self.run_job(job, start=start):
                has_errors = True

        complete = round(perf_counter() - start)
        if has_errors:
            status_message('Completed with errors! {:d}s'.format(complete))
        else:
            status_message('Complete! {:d}s'.format(complete))

    def map_urls_to_files(self, urls: Iterator[QUrl]) -> Iterator[str]:
        for url in urls:
            yield url.toLocalFile()

    def set_ffmpeg_bin(self, path: str) -> None:
        if not path:
            return

        self.path_ffmpeg = path
        self.ffmpeg_control.setText(path)

    def remove_files(self) -> None:
        indexes = sorted(
            map(lambda i: i.row(), self.files_list.selectedIndexes()),
            reverse=True)

        for idx in indexes:
            self.files_model.removeRow(idx)

    def set_files(self, files: Iterator[str]) -> None:
        for file in files:
            file_model = QtGui.QStandardItem(file)
            self.files_model.appendRow(file_model)

    def set_optzoom(self, value: int) -> None:
        optzoom_option = self.optzoom_control.findData(value)
        self.optzoom_control.setCurrentIndex(optzoom_option)

    def show_error_dialog(self, title: str, summary: str, detail: str) -> int:
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)

        msg.setWindowTitle(title)
        msg.setText(summary)
        msg.setInformativeText(detail)
        msg.setStandardButtons(QMessageBox.Ok)

        ret_val = msg.exec()

        return ret_val

    def validate_params(self, state: AppState) -> None:
        if not state.bin.ffmpeg:
            raise RuntimeError(AttrDict(
                type='ffmpeg binary',
                detail='ffmpeg binary is required.',
            ))

        if not os.path.exists(state.bin.ffmpeg):
            raise RuntimeError(AttrDict(
                type='ffmpeg binary',
                detail='ffmpeg binary is invalid (check file path).',
            ))
