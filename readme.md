Ffmpeg Video Stabilizer
---

Python Gui to integrate with `ffmpeg` and `vidstab` plugin.

Requires:
  * `ffmpeg` (with `vidstab` configured in build.
  * `PyQt5`
  * `Python 3.6.9`

## History

Built with quarantine boredom.

Went from an original concept as a simple command line wrapper, 
to a more dynamic config-driven UI.

## Future

As time permits:

* Clean up the UI.
* Add some testing.
* Clean up filter interface.
* Clean up code conventions, organization, structure, naming...

  
## PyQt5
* Install Qt5 and Qt5 Designer

    https://pythonbasics.org/qt-designer-python/
    ```bash
    apt-get install qttools5-dev-tools qttools5-dev python3-pyqt5
    ```

* Run Qt5 Designer

    Use the application launcher. I could not get it to launch
    via the command line.

* System Icons

    https://joekuan.wordpress.com/2015/09/23/list-of-qt-icons/

## Generate UI Code
```bash
pip install pyqt5
```

```bash
pyuic5 video_stabilizer.ui -o auto_ui.py
```

## Permission Errors

`snap` package manager does not give `ffmpeg` access to removable media. You'll
need to give it access if you want to do operations on removable media.

https://unix.stackexchange.com/a/580420

```bash
sudo snap connect ffmpeg:removable-media
```

## Run the application.

Note: You *might* need to install `qt5` on your system.
 
Example `apt-get install python3-pyqt5`

```bash
pip install -r requirements.txt
./app.py
```

## Filter support.

Just drop a `yaml` file in the `filters/` directory in the vein of the 
current filter design; eg `filters/eq.yaml`.

Fair warning: filter support is very basic in this iteration.
