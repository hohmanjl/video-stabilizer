#!/usr/bin/env python3

import sys

from PyQt5.QtWidgets import QApplication

from window import Window


def main():
    app = QApplication(sys.argv)
    window = Window()
    window.show()

    app.exec_()


if __name__ == '__main__':
    main()
